# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/devops/Documents/mytimer/timer.cpp" "/home/devops/Documents/mytimer/build/CMakeFiles/timer.dir/timer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/devops/.conan/data/Poco/1.7.8p3/pocoproject/stable/package/adc07af7ea8a81a3f68fa3f033d07d429a48706f/include"
  "/home/devops/.conan/data/OpenSSL/1.0.2l/conan/stable/package/93a39911b86ec3005dfe8be5be3b85a2792590e2/include"
  "/home/devops/.conan/data/zlib/1.2.11/conan/stable/package/500a5737cfb7bee13d4a0039e72446892ca242ab/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
